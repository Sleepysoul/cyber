// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS
// CW Coleman 211025

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN 6
#define NUM_LEDS 16
#define BRIGHTNESS 50

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()
int led1,led2,led3, red, green, blue;
int wait = 1000;//once again, this variable seems hardly useful
int white = 0;
void loop() {
// turn on leds 
  led1 = random(16);
  led2 = led1 % 100;
  led3 =  led1 % 100;
  red = random(255,255);//you're pink now! thats my attack
  green = random(0,0);
  blue = random(0,0);
  //white = random(255,0);
  white = 100;

    strip.setPixelColor(led1, red, green , blue, white);
    strip.setPixelColor(led2, red, green , blue, white);
    strip.setPixelColor(led3, red, green , blue, white);
    strip.show();
   delay(0);//it looks like its spinnning!!~
   //this loop sets all leds to black
   for ( led1 = 0; led1 < 16; led1++){  
    strip.setPixelColor(led1, 0,0,0,0);
  }//end of for loop
    strip.show();


}
