//This sets the pin's globally. integers can only be whole numbers. this is C programming, not C+
int redPin = 4;
int greenPin = 3;
int bluePin = 2;

void setup() {
  Serial.begin(9600);//this is for debugging, serial out
  pinMode(redPin, OUTPUT);//sets up the green red and blue pin's will be output
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
//initialize global variables outside of the loop. this is a function with no perameters??? the curly bracket makes the encapsulated block
int n = 0;
int count;

void loop() {
  setColor(255,0 ,0);//set color saves space by doing it in one line. this is red
  delay(200);
  setColor(255,30,0);//Orange
  delay(200); 
  setColor(255, 90, 0);//Yellow
   delay(200);
   setColor(0, 255, 0);//Green
   delay(200);
   setColor(0, 255, 255);//Cyan
   delay(200);
    setColor(0, 0, 255);//Blue
   delay(200);
   setColor(255, 0, 255);//Purple
   delay(200);
   setColor(255, 255, 255);//White
   delay(200);
   Serial.print(n);
   Serial.print(" ");
   count++;
  if (count % 20 == 0)Serial.println();
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
